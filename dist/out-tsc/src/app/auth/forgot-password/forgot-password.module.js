import { __decorate } from "tslib";
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgotPasswordRoutingModule } from './forgot-password-routing.module';
import { ForgotPasswordComponent } from './forgot-password.component';
let ForgotPasswordModule = class ForgotPasswordModule {
};
ForgotPasswordModule = __decorate([
    NgModule({
        declarations: [ForgotPasswordComponent],
        imports: [CommonModule, ForgotPasswordRoutingModule, ReactiveFormsModule],
    })
], ForgotPasswordModule);
export { ForgotPasswordModule };
//# sourceMappingURL=forgot-password.module.js.map