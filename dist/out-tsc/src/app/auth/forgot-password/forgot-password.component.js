import { __awaiter, __decorate } from "tslib";
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
let ForgotPasswordComponent = class ForgotPasswordComponent {
    constructor(authSvc, router) {
        this.authSvc = authSvc;
        this.router = router;
        this.userEmail = new FormControl('');
    }
    onReset() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const email = this.userEmail.value;
                yield this.authSvc.resetPassword(email);
                window.alert('Email sent, check your inbox!');
                this.router.navigate(['/login']);
            }
            catch (error) {
                console.log(error);
            }
        });
    }
};
ForgotPasswordComponent = __decorate([
    Component({
        selector: 'app-forgot-password',
        templateUrl: './forgot-password.component.html',
        styleUrls: ['./forgot-password.component.css'],
    })
], ForgotPasswordComponent);
export { ForgotPasswordComponent };
//# sourceMappingURL=forgot-password.component.js.map