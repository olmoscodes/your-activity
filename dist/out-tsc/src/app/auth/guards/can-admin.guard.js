import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { tap, map, take } from 'rxjs/operators';
let CanAdminGuard = class CanAdminGuard {
    constructor(authSvc, router) {
        this.authSvc = authSvc;
        this.router = router;
    }
    canActivate() {
        return this.authSvc.user$.pipe(take(1), map((user) => user && this.authSvc.isAdmin(user)), tap((canEdit) => {
            if (!canEdit) {
                window.alert('Accés denegat. Has de tenir permisos.');
                this.router.navigate(['/login']);
            }
        }));
    }
};
CanAdminGuard = __decorate([
    Injectable({
        providedIn: 'root',
    })
], CanAdminGuard);
export { CanAdminGuard };
//# sourceMappingURL=can-admin.guard.js.map