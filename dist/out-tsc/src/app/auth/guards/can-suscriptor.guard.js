import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { tap, map, take } from 'rxjs/operators';
let CanSuscriptorGuard = class CanSuscriptorGuard {
    constructor(authSvc, router) {
        this.authSvc = authSvc;
        this.router = router;
    }
    canActivate() {
        return this.authSvc.user$.pipe(take(1), map((user) => user && this.authSvc.isSuscriptor(user)), tap((canEdit) => {
            if (!canEdit) {
                window.alert('Accés denegat. Has de tenir permisos.');
                this.router.navigate(['/login']);
            }
        }));
    }
};
CanSuscriptorGuard = __decorate([
    Injectable({
        providedIn: 'root',
    })
], CanSuscriptorGuard);
export { CanSuscriptorGuard };
//# sourceMappingURL=can-suscriptor.guard.js.map