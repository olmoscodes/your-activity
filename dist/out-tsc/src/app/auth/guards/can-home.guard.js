import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { tap, map, take } from 'rxjs/operators';
let CanHomeGuard = class CanHomeGuard {
    constructor(authSvc, router) {
        this.authSvc = authSvc;
        this.router = router;
    }
    //   canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    //     return this.authSvc.UserIsLogged;
    //   }
    // }
    canActivate() {
        return this.authSvc.user$.pipe(take(1), map((user) => user && this.authSvc.isAdmin(user)), tap((canView) => {
            if (!canView) {
                this.router.navigate(['/login']);
            }
            else {
                this.router.navigate(['/suscriptor']);
            }
        }));
    }
};
CanHomeGuard = __decorate([
    Injectable({
        providedIn: 'root',
    })
], CanHomeGuard);
export { CanHomeGuard };
//# sourceMappingURL=can-home.guard.js.map