import { __decorate } from "tslib";
import { Component } from '@angular/core';
let SendEmailComponent = class SendEmailComponent {
    constructor(authSvc) {
        this.authSvc = authSvc;
        this.user$ = this.authSvc.afAuth.user;
    }
    onSendEmail() {
        this.authSvc.sendVerificationEmail();
    }
    ngOnDestroy() {
        this.authSvc.logout();
    }
};
SendEmailComponent = __decorate([
    Component({
        selector: 'app-send-email',
        templateUrl: './send-email.component.html',
        styleUrls: ['./send-email.component.css'],
    })
], SendEmailComponent);
export { SendEmailComponent };
//# sourceMappingURL=send-email.component.js.map