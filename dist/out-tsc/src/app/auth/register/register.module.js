import { __decorate } from "tslib";
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterRoutingModule } from '@auth/register/register-routing.module';
import { RegisterComponent } from '@auth/register/register.component';
let RegisterModule = class RegisterModule {
};
RegisterModule = __decorate([
    NgModule({
        declarations: [RegisterComponent],
        imports: [CommonModule, RegisterRoutingModule, ReactiveFormsModule],
    })
], RegisterModule);
export { RegisterModule };
//# sourceMappingURL=register.module.js.map