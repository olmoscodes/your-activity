import { __awaiter, __decorate } from "tslib";
import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
let RegisterComponent = class RegisterComponent {
    constructor(authSvc, router) {
        this.authSvc = authSvc;
        this.router = router;
        this.registerForm = new FormGroup({
            email: new FormControl(''),
            password: new FormControl(''),
        });
    }
    onRegister() {
        return __awaiter(this, void 0, void 0, function* () {
            const { email, password } = this.registerForm.value;
            try {
                const user = yield this.authSvc.register(email, password);
                if (user) {
                    this.checkUserIsVerified(user);
                }
            }
            catch (error) {
                console.log(error);
            }
        });
    }
    checkUserIsVerified(user) {
        if (user && user.emailVerified) {
            this.router.navigate(['/home']);
        }
        else if (user) {
            this.router.navigate(['/verification-email']);
        }
        else {
            this.router.navigate(['/register']);
        }
    }
};
RegisterComponent = __decorate([
    Component({
        selector: 'app-register',
        templateUrl: './register.component.html',
        styleUrls: ['./register.component.css'],
    })
], RegisterComponent);
export { RegisterComponent };
//# sourceMappingURL=register.component.js.map