import { __awaiter, __decorate } from "tslib";
import { FormGroup, FormControl } from '@angular/forms';
import { Component } from '@angular/core';
let LoginComponent = class LoginComponent {
    constructor(authSvc, router) {
        this.authSvc = authSvc;
        this.router = router;
        this.loginForm = new FormGroup({
            email: new FormControl(''),
            password: new FormControl(''),
        });
    }
    onGoogleLogin() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const user = yield this.authSvc.loginGoogle();
                if (user) {
                    this.checkUserIsVerified(user);
                }
            }
            catch (error) {
                console.log(error);
            }
        });
    }
    onLogin() {
        return __awaiter(this, void 0, void 0, function* () {
            const { email, password } = this.loginForm.value;
            try {
                const user = yield this.authSvc.login(email, password);
                if (user) {
                    this.checkUserIsVerified(user);
                }
            }
            catch (error) {
                console.log(error);
            }
        });
    }
    checkUserIsVerified(user) {
        if (user && user.emailVerified) {
            this.router.navigate(['/home']);
        }
        else if (user) {
            this.router.navigate(['/verification-email']);
        }
        else {
            this.router.navigate(['/register']);
        }
    }
};
LoginComponent = __decorate([
    Component({
        selector: 'app-login',
        templateUrl: './login.component.html',
        styleUrls: ['./login.component.css'],
    })
], LoginComponent);
export { LoginComponent };
//# sourceMappingURL=login.component.js.map