import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginRoutingModule } from '@auth/login/login-routing.module';
import { LoginComponent } from '@auth/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
let LoginModule = class LoginModule {
};
LoginModule = __decorate([
    NgModule({
        declarations: [LoginComponent],
        imports: [CommonModule, LoginRoutingModule, ReactiveFormsModule],
    })
], LoginModule);
export { LoginModule };
//# sourceMappingURL=login.module.js.map