import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
let ActivitatsService = class ActivitatsService {
    constructor(fireStore) {
        this.fireStore = fireStore;
    }
    // Fetch all students information.
    getAllStudents() {
        // valueChanges() function returns the observable containing the student details expect the id of the document.
        // snapshotChanges() function returns the observable containing the student details and the id of the document (i.e. the metadata).
        return this.fireStore.collection('activitats').snapshotChanges();
        // We will use the id in order to perform the update or delete operation.
    }
};
ActivitatsService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], ActivitatsService);
export { ActivitatsService };
//# sourceMappingURL=youractivity.service.js.map