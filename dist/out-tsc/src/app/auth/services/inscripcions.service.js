import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
let InscripcionsService = class InscripcionsService {
    constructor(fireStore, authSvc) {
        this.fireStore = fireStore;
        this.authSvc = authSvc;
    }
    // Fetch all students information.
    getAllInscripcions() {
        return this.fireStore.collection('inscripcions', ref => ref.orderBy("nomActivitat")).snapshotChanges();
    }
};
InscripcionsService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], InscripcionsService);
export { InscripcionsService };
//# sourceMappingURL=inscripcions.service.js.map