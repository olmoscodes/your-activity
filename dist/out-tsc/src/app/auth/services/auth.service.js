import { __awaiter, __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { auth } from 'firebase/app';
import { of } from 'rxjs';
import { switchMap, first } from 'rxjs/operators';
import { RoleValidator } from '@auth/helpers/roleValidator';
let AuthService = class AuthService extends RoleValidator {
    constructor(afAuth, afs, router) {
        super();
        this.afAuth = afAuth;
        this.afs = afs;
        this.router = router;
        this.user$ = this.afAuth.authState.pipe(switchMap((user) => {
            if (user) {
                return this.afs.doc(`users/${user.uid}`).valueChanges();
            }
            return of(null);
        }));
    }
    loginGoogle() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { user } = yield this.afAuth.signInWithPopup(new auth.GoogleAuthProvider());
                this.updateUserData(user);
                return user;
            }
            catch (error) {
                console.log(error);
            }
        });
    }
    resetPassword(email) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return this.afAuth.sendPasswordResetEmail(email);
            }
            catch (error) {
                console.log(error);
            }
        });
    }
    sendVerificationEmail() {
        return __awaiter(this, void 0, void 0, function* () {
            return (yield this.afAuth.currentUser).sendEmailVerification();
        });
    }
    login(email, password) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { user } = yield this.afAuth.signInWithEmailAndPassword(email, password);
                this.updateUserData(user);
                return user;
            }
            catch (error) {
                console.log(error);
            }
        });
    }
    register(email, password) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { user } = yield this.afAuth.createUserWithEmailAndPassword(email, password);
                yield this.sendVerificationEmail();
                return user;
            }
            catch (error) {
                console.log(error);
            }
        });
    }
    logout() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.afAuth.signOut();
            }
            catch (error) {
                console.log(error);
            }
        });
    }
    updateUserData(user) {
        const userRef = this.afs.doc(`users/${user.uid}`);
        const data = {
            uid: user.uid,
            email: user.email,
            emailVerified: user.emailVerified,
            displayName: user.displayName,
            photoURL: user.photoURL,
            role: 'ADMIN',
        };
        return userRef.set(data, { merge: true });
    }
    getCurrentUser() {
        return this.afAuth.authState.pipe(first()).toPromise();
    }
    isLogged() {
        if (this.getCurrentUser()) {
            this.UserIsLogged = true;
            console.log('USER LOGEADO');
        }
        else {
            this.UserIsLogged = false;
            console.log('USER NO LOGEADO');
        }
    }
};
AuthService = __decorate([
    Injectable({ providedIn: 'root' })
], AuthService);
export { AuthService };
//# sourceMappingURL=auth.service.js.map