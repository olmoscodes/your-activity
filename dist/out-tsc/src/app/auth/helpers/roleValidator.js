export class RoleValidator {
    isSuscriptor(user) {
        return user.role === 'SUSCRIPTOR';
    }
    isAdmin(user) {
        return user.role === 'ADMIN';
    }
}
//# sourceMappingURL=roleValidator.js.map