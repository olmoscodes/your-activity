import { __decorate } from "tslib";
import { CanAdminGuard } from '@auth/guards/can-admin.guard';
import { SendEmailComponent } from '@auth/send-email/send-email.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { GestioActComponent } from './admin/gestio-act/gestio-act.component';
import { ComptabilitatComponent } from './admin/comptabilitat/comptabilitat.component';
import { InscripcionsComponent } from './admin/inscripcions/inscripcions.component';
import { CanHomeGuard } from './auth/guards/can-home.guard';
const routes = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full',
    },
    {
        path: 'home',
        loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
        canActivate: [CanHomeGuard],
    },
    {
        path: 'login',
        loadChildren: () => import('./auth/login/login.module').then((m) => m.LoginModule),
    },
    {
        path: 'register',
        loadChildren: () => import('./auth/register/register.module').then((m) => m.RegisterModule),
    },
    {
        path: 'verification-email',
        component: SendEmailComponent,
    },
    {
        path: 'forgot-password',
        loadChildren: () => import('./auth/forgot-password/forgot-password.module').then((m) => m.ForgotPasswordModule),
    },
    {
        path: 'admin',
        loadChildren: () => import('./admin/admin.module').then((m) => m.AdminModule),
        canActivate: [CanAdminGuard],
    },
    {
        path: 'admin/gestio',
        component: GestioActComponent,
        canActivate: [CanAdminGuard],
    },
    {
        path: 'admin/comptabilitat',
        component: ComptabilitatComponent,
        canActivate: [CanAdminGuard],
    },
    {
        path: 'admin/inscripcions',
        component: InscripcionsComponent,
        canActivate: [CanAdminGuard],
    },
    {
        path: 'suscriptor',
        loadChildren: () => import('./suscriptor/suscriptor.module').then((m) => m.SuscriptorModule),
    },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forRoot(routes)],
        exports: [RouterModule],
    })
], AppRoutingModule);
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map