import { __decorate } from "tslib";
import { Component } from '@angular/core';
let InscripcionsComponent = class InscripcionsComponent {
    constructor(service, fireStore) {
        this.service = service;
        this.fireStore = fireStore;
    }
    ngOnInit() {
        this.service.getAllInscripcions().subscribe(response => {
            this.inscripcionsList = response.map(document => {
                return Object.assign({ id: document.payload.doc.id }, document.payload.doc.data());
            });
        });
    }
};
InscripcionsComponent = __decorate([
    Component({
        selector: 'app-inscripcions',
        templateUrl: './inscripcions.component.html',
        styleUrls: ['./inscripcions.component.css']
    })
], InscripcionsComponent);
export { InscripcionsComponent };
//# sourceMappingURL=inscripcions.component.js.map