import { __decorate } from "tslib";
import { Component } from '@angular/core';
let StudentListComponent = class StudentListComponent {
    constructor(service, fireStore) {
        this.service = service;
        this.fireStore = fireStore;
    }
    ngOnInit() {
        this.service.getAllStudents().subscribe(response => {
            this.studentList = response.map(document => {
                return Object.assign({ id: document.payload.doc.id }, document.payload.doc.data() // Attention - Require typescript version >3 to work!! AHORA ENTIENDO POR QUE NO IBA
                );
            });
            // Sorting the student-list in ascending order
            this.studentList = this.studentList.sort((obj1, obj2) => obj1.rollNo - obj2.rollNo);
        });
    }
    onEdit(student) {
        this.service.formData = Object.assign({}, student);
    }
    onDelete(student) {
        this.fireStore.doc('activitats/' + student.id).delete();
        this.deleteMessage = 'La informació de ' + student.nomActivitat + ' ha sigut eliminada correctament';
    }
};
StudentListComponent = __decorate([
    Component({
        selector: 'app-llistaractivitats',
        templateUrl: './llistaractivitats.component.html',
        styleUrls: ['./llistaractivitats.component.css']
    })
], StudentListComponent);
export { StudentListComponent };
//# sourceMappingURL=llistaractivitats.component.js.map