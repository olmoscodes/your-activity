import { __decorate } from "tslib";
import { Component } from '@angular/core';
let StudentComponent = class StudentComponent {
    constructor(service, fireStore) {
        this.service = service;
        this.fireStore = fireStore;
    }
    ngOnInit() {
        this.resetForm();
    }
    resetForm(form) {
        if (form != null) {
            form.resetForm();
        }
        this.service.formData = {
            id: null,
            nomActivitat: '',
            nomMonitor: '',
            descripcioActivitat: '',
            horariActivitatInici: '',
            horariActivitatFinal: '',
        };
    }
    onSubmit(form) {
        // Reset the message value.
        this.message = '';
        // Making the copy of the form and assigning it to the studentData.
        let studentData = Object.assign({}, form.value);
        // To avoid messing up the document id and just update the other details of the student. We will remove the 'property' from the student data.
        delete studentData.id;
        // Does the insert operation.
        if (form.value.id == null) {
            this.fireStore.collection('activitats').add(studentData);
            this.message = studentData.nomActivitat + ' ha sigut guardat/a correctament ';
        }
        else {
            // Does the update operation for the selected student.
            // The 'studentData' object has the updated details of the student.
            this.fireStore.doc('activitats/' + form.value.id).update(studentData);
            this.message = 'Activitat editada correctament';
        }
        // Reset the form if the operation is successful.
        this.resetForm(form);
    }
};
StudentComponent = __decorate([
    Component({
        selector: 'app-afegiractivitats',
        templateUrl: './afegiractivitats.component.html',
        styleUrls: ['./afegiractivitats.component.css']
    })
], StudentComponent);
export { StudentComponent };
//# sourceMappingURL=afegiractivitats.component.js.map