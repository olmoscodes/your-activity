import { __awaiter, __decorate } from "tslib";
import { Component } from '@angular/core';
import { AuthService } from '@app/auth/services/auth.service';
let InscriureActivitatsComponent = class InscriureActivitatsComponent {
    constructor(service, fireStore, serviceInscripcions, authSvc) {
        this.service = service;
        this.fireStore = fireStore;
        this.serviceInscripcions = serviceInscripcions;
        this.authSvc = authSvc;
    }
    ngOnInit() {
        return __awaiter(this, void 0, void 0, function* () {
            // Cada vegada que carrego la pàgina actualitzo el formulari
            this.resetForm();
            // Agafo l'usuari que està actualment registrat
            const user = yield this.authSvc.getCurrentUser();
            // El fico en una variable per que quedi registrat qui registra als nens
            this.emailRegistrat = user.email;
            //Afago totes les activitats per ficar-les com a input en el formulari
            this.service.getAllStudents().subscribe(response => {
                this.ActivitatsLlista = response.map(document => {
                    return Object.assign({ id: document.payload.doc.id }, document.payload.doc.data() // Attention - Require typescript version >3 to work!! AHORA ENTIENDO POR QUE NO IBA
                    );
                });
                // Sorting the student-list in ascending order
                this.ActivitatsLlista = this.ActivitatsLlista.sort((obj1, obj2) => obj1.rollNo - obj2.rollNo);
            });
        });
    }
    // Funció de resetejar el formulari
    resetForm(form) {
        return __awaiter(this, void 0, void 0, function* () {
            if (form != null) {
                form.resetForm();
            }
            this.serviceInscripcions.formDataInscripcions = {
                id: null,
                idActivitat: '',
                nomActivitat: '',
                nomNen: '',
                cognomsNen: '',
                curs: '',
                nomTutor: '',
                telf: '',
                email: ''
            };
            // Torno a ficar el id user dintre de la variable, ja que quan formatejava el formulari sense tornar a carregar
            // la pàgina la següent inserció a la taula de inscripción em sortia email: null. Això ho arregla.
            const user = yield this.authSvc.getCurrentUser();
            this.emailRegistrat = user.email;
        });
    }
    // Funció per incloure informació a al bbdd a partir del formulari
    onSubmit(form) {
        // Reset the message value.
        this.message = '';
        // Making the copy of the form and assigning it to the studentData.
        let InscripcioData = Object.assign({}, form.value);
        // To avoid messing up the document id and just update the other details of the student. We will remove the 'property' from the student data.
        delete InscripcioData.id;
        // Does the insert operation.
        if (form.value.id == null) {
            this.fireStore.collection('inscripcions').add(InscripcioData);
            this.message = InscripcioData.nomActivitat + ' ha sigut guardat/a correctament ';
        }
        else {
            // Does the update operation for the selected student.
            // The 'studentData' object has the updated details of the student.
            this.fireStore.doc('inscripcions/' + form.value.id).update(InscripcioData);
            this.message = 'Inscripcions editada correctament';
        }
        // Reset the form if the operation is successful.
        this.resetForm(form);
    }
};
InscriureActivitatsComponent = __decorate([
    Component({
        selector: 'app-inscriure-activitats',
        templateUrl: './inscriure-activitats.component.html',
        styleUrls: ['./inscriure-activitats.component.css'],
        providers: [AuthService],
    })
], InscriureActivitatsComponent);
export { InscriureActivitatsComponent };
//# sourceMappingURL=inscriure-activitats.component.js.map