import { async, TestBed } from '@angular/core/testing';
import { InscriureActivitatsComponent } from './inscriure-activitats.component';
describe('InscriureActivitatsComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [InscriureActivitatsComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(InscriureActivitatsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=inscriure-activitats.component.spec.js.map