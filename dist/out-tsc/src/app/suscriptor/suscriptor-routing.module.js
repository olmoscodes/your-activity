import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SuscriptorComponent } from './suscriptor.component';
const routes = [{ path: '', component: SuscriptorComponent }];
let SuscriptorRoutingModule = class SuscriptorRoutingModule {
};
SuscriptorRoutingModule = __decorate([
    NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
    })
], SuscriptorRoutingModule);
export { SuscriptorRoutingModule };
//# sourceMappingURL=suscriptor-routing.module.js.map