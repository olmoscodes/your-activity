import { async, TestBed } from '@angular/core/testing';
import { VeureActivitatsComponent } from './veure-activitats.component';
describe('VeureActivitatsComponent', () => {
    let component;
    let fixture;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [VeureActivitatsComponent]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = TestBed.createComponent(VeureActivitatsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=veure-activitats.component.spec.js.map