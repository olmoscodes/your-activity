import { __awaiter, __decorate } from "tslib";
import { Component } from '@angular/core';
let VeureActivitatsComponent = class VeureActivitatsComponent {
    constructor(service, fireStore, authSvc) {
        this.service = service;
        this.fireStore = fireStore;
        this.authSvc = authSvc;
    }
    ngOnInit() {
        return __awaiter(this, void 0, void 0, function* () {
            // Torno a extreure quin usuari está amb la sessió iniciada i el fico dintre de la variable userNOW
            const user = yield this.authSvc.getCurrentUser();
            this.userNOW = user.email;
            //Recullo les dades de les inscripcions filtrades per la variable userNOW (usuari amb la sessió iniciada)
            this.fireStore.collection('inscripcions', ref => ref.where("email", "==", this.userNOW)).snapshotChanges().subscribe(response => {
                this.inscripcionsLlista = response.map(document => {
                    return Object.assign({ id: document.payload.doc.id }, document.payload.doc.data());
                });
                // Ordeno els resultats en ordre ascendent
                this.inscripcionsLlista = this.inscripcionsLlista.sort((obj1, obj2) => obj1.rollNo - obj2.rollNo);
            });
        });
    }
};
VeureActivitatsComponent = __decorate([
    Component({
        selector: 'app-veure-activitats',
        templateUrl: './veure-activitats.component.html',
        styleUrls: ['./veure-activitats.component.css']
    })
], VeureActivitatsComponent);
export { VeureActivitatsComponent };
//# sourceMappingURL=veure-activitats.component.js.map