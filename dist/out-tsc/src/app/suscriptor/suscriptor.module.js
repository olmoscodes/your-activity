import { __decorate } from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuscriptorRoutingModule } from './suscriptor-routing.module';
import { SuscriptorComponent } from './suscriptor.component';
import { VeureActivitatsComponent } from './veure-activitats/veure-activitats.component';
import { InscriureActivitatsComponent } from './inscriure-activitats/inscriure-activitats.component';
import { FormsModule } from '@angular/forms';
let SuscriptorModule = class SuscriptorModule {
};
SuscriptorModule = __decorate([
    NgModule({
        declarations: [SuscriptorComponent, VeureActivitatsComponent, InscriureActivitatsComponent],
        imports: [
            CommonModule,
            SuscriptorRoutingModule,
            FormsModule
        ]
    })
], SuscriptorModule);
export { SuscriptorModule };
//# sourceMappingURL=suscriptor.module.js.map