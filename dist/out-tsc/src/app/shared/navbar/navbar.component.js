import { __awaiter, __decorate } from "tslib";
import { Component } from '@angular/core';
let NavbarComponent = class NavbarComponent {
    constructor(authSvc, router) {
        this.authSvc = authSvc;
        this.router = router;
        this.user$ = this.authSvc.afAuth.user;
        this.navbarvisible = false;
        this.botoamaga = false;
        this.botomostra = true;
    }
    // Amb això evitem l'entrada a diferents 
    ngOnInit() {
        return __awaiter(this, void 0, void 0, function* () {
            this.adminOrNot = yield this.authSvc.getCurrentUser();
            if (this.adminOrNot) {
                console.log('hi ha usuari amb sessió iniciada');
            }
            else {
                this.router.navigate(['/login']);
            }
        });
    }
    onLogout() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.authSvc.logout();
                this.router.navigate(['/login']);
            }
            catch (error) {
                console.log(error);
            }
        });
    }
};
NavbarComponent = __decorate([
    Component({
        selector: 'app-navbar',
        templateUrl: './navbar.component.html',
        styleUrls: ['./navbar.component.css'],
    })
], NavbarComponent);
export { NavbarComponent };
//# sourceMappingURL=navbar.component.js.map