function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["suscriptor-suscriptor-module"], {
  /***/
  "./src/app/suscriptor/inscriure-activitats/inscriure-activitats.component.ts":
  /*!***********************************************************************************!*\
    !*** ./src/app/suscriptor/inscriure-activitats/inscriure-activitats.component.ts ***!
    \***********************************************************************************/

  /*! exports provided: InscriureActivitatsComponent */

  /***/
  function srcAppSuscriptorInscriureActivitatsInscriureActivitatsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InscriureActivitatsComponent", function () {
      return InscriureActivitatsComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _app_auth_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @app/auth/services/auth.service */
    "./src/app/auth/services/auth.service.ts");
    /* harmony import */


    var _app_auth_services_youractivity_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @app/auth/services/youractivity.service */
    "./src/app/auth/services/youractivity.service.ts");
    /* harmony import */


    var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/fire/firestore */
    "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
    /* harmony import */


    var _app_auth_services_inscripcions_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @app/auth/services/inscripcions.service */
    "./src/app/auth/services/inscripcions.service.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    function InscriureActivitatsComponent_div_8_Template(rf, ctx) {
      if (rf & 1) {
        var _r18 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "input", 41, 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function InscriureActivitatsComponent_div_8_Template_input_ngModelChange_1_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r18);

          var ctx_r17 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r17.serviceInscripcions.formDataInscripcions.nomActivitat = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "label", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "div", 44);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 45);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 46);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "div", 48);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "div", 49);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "div", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "div", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "div", 52);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var opcioInfo_r14 = ctx.$implicit;
        var i_r15 = ctx.index;

        var ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("id", i_r15);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("value", opcioInfo_r14.nomActivitat);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx_r1.serviceInscripcions.formDataInscripcions.nomActivitat);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpropertyInterpolate"]("for", i_r15);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](opcioInfo_r14.nomActivitat);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](opcioInfo_r14.descripcioActivitat);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate2"](" ", opcioInfo_r14.horariActivitatInici, "h - ", opcioInfo_r14.horariActivitatFinal, "h ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", opcioInfo_r14.preuActivitat, " \u20AC");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](opcioInfo_r14.nomMonitor);
      }
    }

    function InscriureActivitatsComponent_div_21_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Es requereix escriure el nom del nen/a.");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function InscriureActivitatsComponent_div_24_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Es requereix escriure el cognom del nen/a.");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function InscriureActivitatsComponent_div_27_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Es requereix escriure nom i cognoms del tutor legal.");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function InscriureActivitatsComponent_div_44_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Es requereix escriure el curs del nen/a.");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function InscriureActivitatsComponent_div_47_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "Es requereix escriure un tel\xE9fon v\xE0lid");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    var InscriureActivitatsComponent = /*#__PURE__*/function () {
      function InscriureActivitatsComponent(service, fireStore, serviceInscripcions, authSvc) {
        _classCallCheck(this, InscriureActivitatsComponent);

        this.service = service;
        this.fireStore = fireStore;
        this.serviceInscripcions = serviceInscripcions;
        this.authSvc = authSvc;
      }

      _createClass(InscriureActivitatsComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
            var _this = this;

            var user;
            return regeneratorRuntime.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    // Cada vegada que carrego la pàgina actualitzo el formulari
                    this.resetForm(); // Agafo l'usuari que està actualment registrat

                    _context.next = 3;
                    return this.authSvc.getCurrentUser();

                  case 3:
                    user = _context.sent;
                    // El fico en una variable per que quedi registrat qui registra als nens
                    this.emailRegistrat = user.email; //Afago totes les activitats per ficar-les com a input en el formulari

                    this.service.getAllStudents().subscribe(function (response) {
                      _this.ActivitatsLlista = response.map(function (document) {
                        return Object.assign({
                          id: document.payload.doc.id
                        }, document.payload.doc.data() // Attention - Require typescript version >3 to work!! AHORA ENTIENDO POR QUE NO IBA
                        );
                      }); // Sorting the student-list in ascending order

                      _this.ActivitatsLlista = _this.ActivitatsLlista.sort(function (obj1, obj2) {
                        return obj1.rollNo - obj2.rollNo;
                      });
                    });

                  case 6:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, this);
          }));
        } // Funció de resetejar el formulari

      }, {
        key: "resetForm",
        value: function resetForm(form) {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
            var user;
            return regeneratorRuntime.wrap(function _callee2$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    if (form != null) {
                      form.resetForm();
                    }

                    this.serviceInscripcions.formDataInscripcions = {
                      id: null,
                      idActivitat: '',
                      nomActivitat: '',
                      nomNen: '',
                      cognomsNen: '',
                      curs: '',
                      nomTutor: '',
                      telf: '',
                      email: ''
                    }; // Torno a ficar el id user dintre de la variable, ja que quan formatejava el formulari sense tornar a carregar
                    // la pàgina la següent inserció a la taula de inscripción em sortia email: null. Això ho arregla.

                    _context2.next = 4;
                    return this.authSvc.getCurrentUser();

                  case 4:
                    user = _context2.sent;
                    this.emailRegistrat = user.email;

                  case 6:
                  case "end":
                    return _context2.stop();
                }
              }
            }, _callee2, this);
          }));
        } // Funció per incloure informació a al bbdd a partir del formulari

      }, {
        key: "onSubmit",
        value: function onSubmit(form) {
          // Reset the message value.
          this.message = ''; // Making the copy of the form and assigning it to the studentData.

          var InscripcioData = Object.assign({}, form.value); // To avoid messing up the document id and just update the other details of the student. We will remove the 'property' from the student data.

          delete InscripcioData.id; // Does the insert operation.

          if (form.value.id == null) {
            this.fireStore.collection('inscripcions').add(InscripcioData);
            this.message = InscripcioData.nomActivitat + ' ha sigut guardat/a correctament ';
          } else {
            // Does the update operation for the selected student.
            // The 'studentData' object has the updated details of the student.
            this.fireStore.doc('inscripcions/' + form.value.id).update(InscripcioData);
            this.message = 'Inscripcions editada correctament';
          } // Reset the form if the operation is successful.


          this.resetForm(form);
        }
      }]);

      return InscriureActivitatsComponent;
    }();

    InscriureActivitatsComponent.ɵfac = function InscriureActivitatsComponent_Factory(t) {
      return new (t || InscriureActivitatsComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_app_auth_services_youractivity_service__WEBPACK_IMPORTED_MODULE_3__["ActivitatsService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_app_auth_services_inscripcions_service__WEBPACK_IMPORTED_MODULE_5__["InscripcionsService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_app_auth_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]));
    };

    InscriureActivitatsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
      type: InscriureActivitatsComponent,
      selectors: [["app-inscriure-activitats"]],
      features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵProvidersFeature"]([_app_auth_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])],
      decls: 51,
      vars: 14,
      consts: [["autocomplete", "off", 1, "formulari", 3, "submit"], ["form", "ngForm"], [1, "avis1"], [1, "avisIcon"], ["aria-hidden", "true", 1, "fa", "fa-exclamation-circle"], [1, "avisText"], [1, "caixaActivitats"], ["class", "caixaOpcio", 4, "ngFor", "ngForOf"], [1, "caixaOpcioFinal"], ["type", "hidden", "name", "id", 3, "ngModel", "ngModelChange"], ["id", "ngModel"], ["type", "hidden", "name", "email", 3, "ngModel", "ngModelChange"], ["email", "ngModel"], ["name", "nomNen", "placeholder", "Nom del nen/a", "required", "", 1, "inputForm", 3, "ngModel", "ngModelChange"], ["nomNen", "ngModel"], ["class", "validation-error", 4, "ngIf"], ["name", "cognomsNen", "placeholder", "Cognoms del nen/a", "required", "", 1, "inputForm", 3, "ngModel", "ngModelChange"], ["cognomsNen", "ngModel"], ["name", "nomTutor", "placeholder", "Nom i cognoms del tutor que recull al nen/a", "required", "", 1, "inputForm", 3, "ngModel", "ngModelChange"], ["nomTutor", "ngModel"], ["list", "cursos", "name", "curs", "placeholder", "Curs del nen/a", "required", "", 1, "inputForm", 3, "ngModel", "ngModelChange"], ["curs", "ngModel"], ["id", "cursos"], ["value", "P3"], ["value", "P4"], ["value", "P5"], ["value", "Primer de prim\xE0ria"], ["value", "Segon de prim\xE0ria"], ["value", "Tercer de prim\xE0ria"], ["value", "Quart de prim\xE0ria"], ["value", "Cinqu\xE8 de prim\xE0ria"], ["value", "Sis\xE8 de prim\xE0ria"], ["value", "1 ESO"], ["value", "2 ESO"], ["value", "3 ESO"], ["value", "4 ESO"], ["name", "telf", "placeholder", "Tel\xE9fon de contacte del tutor", "pattern", "[0-9]{9}", "required", "", 1, "inputForm", 3, "ngModel", "ngModelChange"], ["telf", "ngModel"], [1, "actFormBoto"], ["type", "submit", 1, "FormBoto", 3, "disabled"], [1, "caixaOpcio"], ["type", "radio", "name", "nomActivitat", "required", "", 3, "id", "value", "ngModel", "ngModelChange"], ["nomActivitat", "ngModel"], [3, "for"], [1, "top"], [1, "caixaActivitatTitol"], [1, "caixaActivitatDescripcio"], [1, "bottom"], [1, "bottom-up"], [1, "caixaActivitatDetallsUp"], [1, "caixaActivitatDetallsUp", "caixaActivitatDetallsUpLateral"], [1, "bottom-down"], [1, "caixaActivitatDetallsDown"], [1, "validation-error"]],
      template: function InscriureActivitatsComponent_Template(rf, ctx) {
        if (rf & 1) {
          var _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "form", 0, 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("submit", function InscriureActivitatsComponent_Template_form_submit_0_listener() {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r19);

            var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](1);

            return ctx.onSubmit(_r0);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "i", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Per visualitzar totes les activitats disponibles llisca de dreta a esquerra");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](8, InscriureActivitatsComponent_div_8_Template, 18, 10, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](9, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](12, "i", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14, "Per realitzar una inscripci\xF3, primer selecciona l'activitat i despr\xE9s completa aquest formulari");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "input", 9, 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function InscriureActivitatsComponent_Template_input_ngModelChange_15_listener($event) {
            return ctx.serviceInscripcions.formDataInscripcions.id = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "input", 11, 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function InscriureActivitatsComponent_Template_input_ngModelChange_17_listener($event) {
            return ctx.emailRegistrat = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "input", 13, 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function InscriureActivitatsComponent_Template_input_ngModelChange_19_listener($event) {
            return ctx.serviceInscripcions.formDataInscripcions.nomNen = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](21, InscriureActivitatsComponent_div_21_Template, 2, 0, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "input", 16, 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function InscriureActivitatsComponent_Template_input_ngModelChange_22_listener($event) {
            return ctx.serviceInscripcions.formDataInscripcions.cognomsNen = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](24, InscriureActivitatsComponent_div_24_Template, 2, 0, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "input", 18, 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function InscriureActivitatsComponent_Template_input_ngModelChange_25_listener($event) {
            return ctx.serviceInscripcions.formDataInscripcions.nomTutor = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](27, InscriureActivitatsComponent_div_27_Template, 2, 0, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "input", 20, 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function InscriureActivitatsComponent_Template_input_ngModelChange_28_listener($event) {
            return ctx.serviceInscripcions.formDataInscripcions.curs = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](30, "datalist", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](31, "option", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](32, "option", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](33, "option", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](34, "option", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](35, "option", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](36, "option", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](37, "option", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](38, "option", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](39, "option", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](40, "option", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](41, "option", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](42, "option", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](43, "option", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](44, InscriureActivitatsComponent_div_44_Template, 2, 0, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](45, "input", 36, 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function InscriureActivitatsComponent_Template_input_ngModelChange_45_listener($event) {
            return ctx.serviceInscripcions.formDataInscripcions.telf = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](47, InscriureActivitatsComponent_div_47_Template, 2, 0, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](48, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](49, "button", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](50, "Realitzar inscripci\xF3");

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](1);

          var _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](20);

          var _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](23);

          var _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](26);

          var _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](29);

          var _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](46);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.ActivitatsLlista);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.serviceInscripcions.formDataInscripcions.id);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.emailRegistrat);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.serviceInscripcions.formDataInscripcions.nomNen);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _r4.invalid && _r4.touched);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.serviceInscripcions.formDataInscripcions.cognomsNen);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _r6.invalid && _r6.touched);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.serviceInscripcions.formDataInscripcions.nomTutor);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _r8.invalid && _r8.touched);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.serviceInscripcions.formDataInscripcions.curs);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](16);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _r10.invalid && _r10.touched);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.serviceInscripcions.formDataInscripcions.telf);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", _r12.invalid && _r12.touched);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", _r0.invalid);
        }
      },
      directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgForm"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["RequiredValidator"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ɵangular_packages_forms_forms_x"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["PatternValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["RadioControlValueAccessor"]],
      styles: [".caixaActivitats[_ngcontent-%COMP%] {\n    display: flex;\n    width: 100%;\n    height: 100%;\n    overflow-x: scroll;\n    \n}\n\nform[_ngcontent-%COMP%] {\n    width: 100%;\n}\n\n.avis1[_ngcontent-%COMP%] {\n    display: flex;\n    border: 2px solid blueviolet;\n    background-color: blueviolet;\n    color: white;\n    padding: 10px;\n    height: auto;\n    width: auto;\n    border-radius: 5px;\n    margin: 10px 20px 10px 10px;\n}\n\n.avisIcon[_ngcontent-%COMP%] {\n    font-size: 30px;\n    margin-right: 10px;\n}\n\n.avisText[_ngcontent-%COMP%] {\n    align-self: center;\n}\n\n.caixaOpcio[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    margin: 10px;\n    min-width: 350px;\n    max-width: 350px;\n    box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.05);\n    border-radius: 5px;\n    background-color: white;\n}\n\n.caixaOpcio[_ngcontent-%COMP%]   label[_ngcontent-%COMP%]{\n    display: flex;\n    flex-direction: column;\n    justify-content: space-between;\n    cursor: pointer;\n    transition: all 0.5s ease 0s;\n    width: 100%;\n    height: 100%;\n    border-radius: 5px;\n    margin: 0px;\n\n}\n\ninput[type=\"radio\"][_ngcontent-%COMP%] {\n\tdisplay: none;\n}\n\ninput[type=\"radio\"][_ngcontent-%COMP%]:checked    + label[_ngcontent-%COMP%] {\n\tbackground: blueviolet;\n    color: white;\n}\n\n.caixaOpcioFinal[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    border: 1px solid white;\n    margin: 10px;\n    height: 300px;\n    width: 20px;    \n    color: white\n}\n\n.top[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n\n}\n\n.bottom[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    height: 100px;\n}\n\n.bottom-up[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row;\n    height: 50%;\n    border-top: 1px solid rgb(231, 231, 231);\n}\n\n.bottom-down[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row;\n    height: 50%;\n    border-top: 1px solid rgb(231, 231, 231);\n}\n\n.caixaActivitatDetallsUp[_ngcontent-%COMP%] {\n    display: flex;\n    padding: 5px;\n    width: 50%;\n    justify-content: center;\n    align-items: center;\n    text-align: center;\n}\n\n.caixaActivitatDetallsUpLateral[_ngcontent-%COMP%] {\n    border-left: 1px solid rgb(231, 231, 231);\n\n}\n\n.caixaActivitatDetallsDown[_ngcontent-%COMP%] {\n    display: flex;\n    padding: 5px;\n    width: 100%;\n    justify-content: center;\n    align-items: center;\n\n    text-align: center;\n}\n\n.caixaActivitatTitol[_ngcontent-%COMP%] {\n    display: flex;\n    font-size: 30px;\n    font-weight: bold;\n    margin: 20px;\n}\n\n.caixaActivitatDescripcio[_ngcontent-%COMP%] {\n    display: flex;\n    margin: 0px 20px 20px 20px;\n}\n\n.caixaActivitatMonitor[_ngcontent-%COMP%] {\n    display: flex;\n    margin: 0px 20px 20px 20px;\n    font-weight: bold;\n}\n\n.inputForm[_ngcontent-%COMP%] {\n    margin: 20px 10px 20px 10px;\n    border: 0px;\n    box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.05);\n    height: 50px;\n    width: 93%;\n    border-radius: 5px;\n    padding-left: 20px;\n}\n\n.validation-error[_ngcontent-%COMP%] {\n    margin: 0px 10px 20px 10px;\n    color: lightcoral;\n}\n\n.FormBoto[_ngcontent-%COMP%]{\n    display: flex;\n    border: 0px;\n    width: auto;\n    align-items: center;\n    justify-content: center;\n    margin: 20px 0px 50px 10px;\n    cursor: pointer;\n    height: 50px;\n    background-color: blueviolet;\n    border-radius: 5px;\n    color: white;\n    font-size: 15px;\n    padding: 20px;\n\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3VzY3JpcHRvci9pbnNjcml1cmUtYWN0aXZpdGF0cy9pbnNjcml1cmUtYWN0aXZpdGF0cy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksYUFBYTtJQUNiLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLDZCQUE2QjtBQUNqQzs7QUFFQTtJQUNJLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGFBQWE7SUFDYiw0QkFBNEI7SUFDNUIsNEJBQTRCO0lBQzVCLFlBQVk7SUFDWixhQUFhO0lBQ2IsWUFBWTtJQUNaLFdBQVc7SUFDWCxrQkFBa0I7SUFDbEIsMkJBQTJCO0FBQy9COztBQUVBO0lBQ0ksZUFBZTtJQUNmLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsWUFBWTtJQUNaLGdCQUFnQjtJQUNoQixnQkFBZ0I7SUFDaEIsNkNBQTZDO0lBQzdDLGtCQUFrQjtJQUNsQix1QkFBdUI7QUFDM0I7O0FBRUE7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLDhCQUE4QjtJQUM5QixlQUFlO0lBQ2YsNEJBQTRCO0lBQzVCLFdBQVc7SUFDWCxZQUFZO0lBQ1osa0JBQWtCO0lBQ2xCLFdBQVc7O0FBRWY7O0FBRUE7Q0FDQyxhQUFhO0FBQ2Q7O0FBRUE7Q0FDQyxzQkFBc0I7SUFDbkIsWUFBWTtBQUNoQjs7QUFHQTtJQUNJLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsdUJBQXVCO0lBQ3ZCLFlBQVk7SUFDWixhQUFhO0lBQ2IsV0FBVztJQUNYO0FBQ0o7O0FBRUE7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCOztBQUUxQjs7QUFFQTtJQUNJLGFBQWE7SUFDYixzQkFBc0I7SUFDdEIsYUFBYTtBQUNqQjs7QUFFQTtJQUNJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLHdDQUF3QztBQUM1Qzs7QUFFQTtJQUNJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsV0FBVztJQUNYLHdDQUF3QztBQUM1Qzs7QUFFQTtJQUNJLGFBQWE7SUFDYixZQUFZO0lBQ1osVUFBVTtJQUNWLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0kseUNBQXlDOztBQUU3Qzs7QUFDQTtJQUNJLGFBQWE7SUFDYixZQUFZO0lBQ1osV0FBVztJQUNYLHVCQUF1QjtJQUN2QixtQkFBbUI7O0lBRW5CLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLGFBQWE7SUFDYixlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLFlBQVk7QUFDaEI7O0FBRUE7SUFDSSxhQUFhO0lBQ2IsMEJBQTBCO0FBQzlCOztBQUVBO0lBQ0ksYUFBYTtJQUNiLDBCQUEwQjtJQUMxQixpQkFBaUI7QUFDckI7O0FBR0E7SUFDSSwyQkFBMkI7SUFDM0IsV0FBVztJQUNYLDZDQUE2QztJQUM3QyxZQUFZO0lBQ1osVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixrQkFBa0I7QUFDdEI7O0FBRUE7SUFDSSwwQkFBMEI7SUFDMUIsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0ksYUFBYTtJQUNiLFdBQVc7SUFDWCxXQUFXO0lBQ1gsbUJBQW1CO0lBQ25CLHVCQUF1QjtJQUN2QiwwQkFBMEI7SUFDMUIsZUFBZTtJQUNmLFlBQVk7SUFDWiw0QkFBNEI7SUFDNUIsa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixlQUFlO0lBQ2YsYUFBYTs7QUFFakIiLCJmaWxlIjoic3JjL2FwcC9zdXNjcmlwdG9yL2luc2NyaXVyZS1hY3Rpdml0YXRzL2luc2NyaXVyZS1hY3Rpdml0YXRzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FpeGFBY3Rpdml0YXRzIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBvdmVyZmxvdy14OiBzY3JvbGw7XG4gICAgLyogYm9yZGVyOiAxcHggc29saWQgZ3JlZW47ICovXG59XG5cbmZvcm0ge1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG4uYXZpczEge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYm9yZGVyOiAycHggc29saWQgYmx1ZXZpb2xldDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBibHVldmlvbGV0O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIGhlaWdodDogYXV0bztcbiAgICB3aWR0aDogYXV0bztcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgbWFyZ2luOiAxMHB4IDIwcHggMTBweCAxMHB4O1xufVxuXG4uYXZpc0ljb24ge1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbi5hdmlzVGV4dCB7XG4gICAgYWxpZ24tc2VsZjogY2VudGVyO1xufVxuXG4uY2FpeGFPcGNpbyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIG1hcmdpbjogMTBweDtcbiAgICBtaW4td2lkdGg6IDM1MHB4O1xuICAgIG1heC13aWR0aDogMzUwcHg7XG4gICAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4IDVweCByZ2JhKDAsMCwwLDAuMDUpO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLmNhaXhhT3BjaW8gbGFiZWx7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgdHJhbnNpdGlvbjogYWxsIDAuNXMgZWFzZSAwcztcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIG1hcmdpbjogMHB4O1xuXG59XG5cbmlucHV0W3R5cGU9XCJyYWRpb1wiXSB7XG5cdGRpc3BsYXk6IG5vbmU7XG59XG5cbmlucHV0W3R5cGU9XCJyYWRpb1wiXTpjaGVja2VkICsgbGFiZWwge1xuXHRiYWNrZ3JvdW5kOiBibHVldmlvbGV0O1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cblxuXG4uY2FpeGFPcGNpb0ZpbmFsIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYm9yZGVyOiAxcHggc29saWQgd2hpdGU7XG4gICAgbWFyZ2luOiAxMHB4O1xuICAgIGhlaWdodDogMzAwcHg7XG4gICAgd2lkdGg6IDIwcHg7ICAgIFxuICAgIGNvbG9yOiB3aGl0ZVxufVxuXG4udG9wIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG5cbn1cblxuLmJvdHRvbSB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGhlaWdodDogMTAwcHg7XG59XG5cbi5ib3R0b20tdXAge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBoZWlnaHQ6IDUwJTtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiKDIzMSwgMjMxLCAyMzEpO1xufVxuXG4uYm90dG9tLWRvd24ge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBoZWlnaHQ6IDUwJTtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiKDIzMSwgMjMxLCAyMzEpO1xufVxuXG4uY2FpeGFBY3Rpdml0YXREZXRhbGxzVXAge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIHdpZHRoOiA1MCU7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5jYWl4YUFjdGl2aXRhdERldGFsbHNVcExhdGVyYWwge1xuICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgcmdiKDIzMSwgMjMxLCAyMzEpO1xuXG59XG4uY2FpeGFBY3Rpdml0YXREZXRhbGxzRG93biB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcblxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmNhaXhhQWN0aXZpdGF0VGl0b2wge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIG1hcmdpbjogMjBweDtcbn1cblxuLmNhaXhhQWN0aXZpdGF0RGVzY3JpcGNpbyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtYXJnaW46IDBweCAyMHB4IDIwcHggMjBweDtcbn1cblxuLmNhaXhhQWN0aXZpdGF0TW9uaXRvciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtYXJnaW46IDBweCAyMHB4IDIwcHggMjBweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuXG4uaW5wdXRGb3JtIHtcbiAgICBtYXJnaW46IDIwcHggMTBweCAyMHB4IDEwcHg7XG4gICAgYm9yZGVyOiAwcHg7XG4gICAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4IDVweCByZ2JhKDAsMCwwLDAuMDUpO1xuICAgIGhlaWdodDogNTBweDtcbiAgICB3aWR0aDogOTMlO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG59XG5cbi52YWxpZGF0aW9uLWVycm9yIHtcbiAgICBtYXJnaW46IDBweCAxMHB4IDIwcHggMTBweDtcbiAgICBjb2xvcjogbGlnaHRjb3JhbDtcbn1cblxuLkZvcm1Cb3Rve1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYm9yZGVyOiAwcHg7XG4gICAgd2lkdGg6IGF1dG87XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBtYXJnaW46IDIwcHggMHB4IDUwcHggMTBweDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG4gICAgaGVpZ2h0OiA1MHB4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IGJsdWV2aW9sZXQ7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgcGFkZGluZzogMjBweDtcblxufVxuXG4iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](InscriureActivitatsComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
          selector: 'app-inscriure-activitats',
          templateUrl: './inscriure-activitats.component.html',
          styleUrls: ['./inscriure-activitats.component.css'],
          providers: [_app_auth_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]]
        }]
      }], function () {
        return [{
          type: _app_auth_services_youractivity_service__WEBPACK_IMPORTED_MODULE_3__["ActivitatsService"]
        }, {
          type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_4__["AngularFirestore"]
        }, {
          type: _app_auth_services_inscripcions_service__WEBPACK_IMPORTED_MODULE_5__["InscripcionsService"]
        }, {
          type: _app_auth_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/suscriptor/suscriptor-routing.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/suscriptor/suscriptor-routing.module.ts ***!
    \*********************************************************/

  /*! exports provided: SuscriptorRoutingModule */

  /***/
  function srcAppSuscriptorSuscriptorRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SuscriptorRoutingModule", function () {
      return SuscriptorRoutingModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _suscriptor_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./suscriptor.component */
    "./src/app/suscriptor/suscriptor.component.ts");

    var routes = [{
      path: '',
      component: _suscriptor_component__WEBPACK_IMPORTED_MODULE_2__["SuscriptorComponent"]
    }];

    var SuscriptorRoutingModule = function SuscriptorRoutingModule() {
      _classCallCheck(this, SuscriptorRoutingModule);
    };

    SuscriptorRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: SuscriptorRoutingModule
    });
    SuscriptorRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function SuscriptorRoutingModule_Factory(t) {
        return new (t || SuscriptorRoutingModule)();
      },
      imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](SuscriptorRoutingModule, {
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SuscriptorRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/suscriptor/suscriptor.component.ts":
  /*!****************************************************!*\
    !*** ./src/app/suscriptor/suscriptor.component.ts ***!
    \****************************************************/

  /*! exports provided: SuscriptorComponent */

  /***/
  function srcAppSuscriptorSuscriptorComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SuscriptorComponent", function () {
      return SuscriptorComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _veure_activitats_veure_activitats_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./veure-activitats/veure-activitats.component */
    "./src/app/suscriptor/veure-activitats/veure-activitats.component.ts");
    /* harmony import */


    var _inscriure_activitats_inscriure_activitats_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./inscriure-activitats/inscriure-activitats.component */
    "./src/app/suscriptor/inscriure-activitats/inscriure-activitats.component.ts");

    var SuscriptorComponent = /*#__PURE__*/function () {
      function SuscriptorComponent() {
        _classCallCheck(this, SuscriptorComponent);
      }

      _createClass(SuscriptorComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return SuscriptorComponent;
    }();

    SuscriptorComponent.ɵfac = function SuscriptorComponent_Factory(t) {
      return new (t || SuscriptorComponent)();
    };

    SuscriptorComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: SuscriptorComponent,
      selectors: [["app-suscriptor"]],
      decls: 9,
      vars: 0,
      consts: [[1, "screen2"], [1, "titol"], [1, "caixaDeCaixes1"], [1, "caixaDeCaixes"]],
      template: function SuscriptorComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Les meves activitats");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "app-veure-activitats");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Afegir activitats");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "app-inscriure-activitats");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }
      },
      directives: [_veure_activitats_veure_activitats_component__WEBPACK_IMPORTED_MODULE_1__["VeureActivitatsComponent"], _inscriure_activitats_inscriure_activitats_component__WEBPACK_IMPORTED_MODULE_2__["InscriureActivitatsComponent"]],
      styles: [".screen2[_ngcontent-%COMP%] {\n    display: flex;\n    margin-top: 100px;\n    width: 100%;\n    height: 100%;\n    background-color: white;\n    flex-direction: column\n}\n\n.titol[_ngcontent-%COMP%] {\n    display: flex;\n    margin: 30px 0px 0px 30px;\n    font-size: 35px;\n    font-weight: bold;\n    color: black;\n}\n\n.caixaDeCaixes1[_ngcontent-%COMP%] {\n    display: flex;\n    margin: 15px 0px 0px 20px;\n    height: auto;\n    width: calc(100% - 30px);\n    \n}\n\n.caixaDeCaixes[_ngcontent-%COMP%] {\n    display: flex;\n    margin: 15px 0px 0px 20px;\n    height: 390px;\n    width: calc(100% - 30px);\n    \n}\n\napp-inscriure-activitats[_ngcontent-%COMP%] {\n    display: flex;\n    width: 100%;\n}\n\napp-veure-activitats[_ngcontent-%COMP%] {\n    display: flex;\n    width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3VzY3JpcHRvci9zdXNjcmlwdG9yLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxhQUFhO0lBQ2IsaUJBQWlCO0lBQ2pCLFdBQVc7SUFDWCxZQUFZO0lBQ1osdUJBQXVCO0lBQ3ZCO0FBQ0o7O0FBRUE7SUFDSSxhQUFhO0lBQ2IseUJBQXlCO0lBQ3pCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsWUFBWTtBQUNoQjs7QUFHQTtJQUNJLGFBQWE7SUFDYix5QkFBeUI7SUFDekIsWUFBWTtJQUNaLHdCQUF3QjtJQUN4Qiw2QkFBNkI7QUFDakM7O0FBRUE7SUFDSSxhQUFhO0lBQ2IseUJBQXlCO0lBQ3pCLGFBQWE7SUFDYix3QkFBd0I7SUFDeEIsNkJBQTZCO0FBQ2pDOztBQUVBO0lBQ0ksYUFBYTtJQUNiLFdBQVc7QUFDZjs7QUFFQTtJQUNJLGFBQWE7SUFDYixXQUFXO0FBQ2YiLCJmaWxlIjoic3JjL2FwcC9zdXNjcmlwdG9yL3N1c2NyaXB0b3IuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zY3JlZW4yIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIG1hcmdpbi10b3A6IDEwMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uXG59XG5cbi50aXRvbCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBtYXJnaW46IDMwcHggMHB4IDBweCAzMHB4O1xuICAgIGZvbnQtc2l6ZTogMzVweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBjb2xvcjogYmxhY2s7XG59XG5cblxuLmNhaXhhRGVDYWl4ZXMxIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIG1hcmdpbjogMTVweCAwcHggMHB4IDIwcHg7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAzMHB4KTtcbiAgICAvKiBib3JkZXI6IDFweCBzb2xpZCBibGFjazsgKi9cbn1cblxuLmNhaXhhRGVDYWl4ZXMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgbWFyZ2luOiAxNXB4IDBweCAwcHggMjBweDtcbiAgICBoZWlnaHQ6IDM5MHB4O1xuICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAzMHB4KTtcbiAgICAvKiBib3JkZXI6IDFweCBzb2xpZCBibGFjazsgKi9cbn1cblxuYXBwLWluc2NyaXVyZS1hY3Rpdml0YXRzIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG5hcHAtdmV1cmUtYWN0aXZpdGF0cyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICB3aWR0aDogMTAwJTtcbn0iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SuscriptorComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-suscriptor',
          templateUrl: './suscriptor.component.html',
          styleUrls: ['./suscriptor.component.css']
        }]
      }], function () {
        return [];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/suscriptor/suscriptor.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/suscriptor/suscriptor.module.ts ***!
    \*************************************************/

  /*! exports provided: SuscriptorModule */

  /***/
  function srcAppSuscriptorSuscriptorModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SuscriptorModule", function () {
      return SuscriptorModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _suscriptor_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./suscriptor-routing.module */
    "./src/app/suscriptor/suscriptor-routing.module.ts");
    /* harmony import */


    var _suscriptor_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./suscriptor.component */
    "./src/app/suscriptor/suscriptor.component.ts");
    /* harmony import */


    var _veure_activitats_veure_activitats_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./veure-activitats/veure-activitats.component */
    "./src/app/suscriptor/veure-activitats/veure-activitats.component.ts");
    /* harmony import */


    var _inscriure_activitats_inscriure_activitats_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./inscriure-activitats/inscriure-activitats.component */
    "./src/app/suscriptor/inscriure-activitats/inscriure-activitats.component.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");

    var SuscriptorModule = function SuscriptorModule() {
      _classCallCheck(this, SuscriptorModule);
    };

    SuscriptorModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: SuscriptorModule
    });
    SuscriptorModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function SuscriptorModule_Factory(t) {
        return new (t || SuscriptorModule)();
      },
      imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _suscriptor_routing_module__WEBPACK_IMPORTED_MODULE_2__["SuscriptorRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](SuscriptorModule, {
        declarations: [_suscriptor_component__WEBPACK_IMPORTED_MODULE_3__["SuscriptorComponent"], _veure_activitats_veure_activitats_component__WEBPACK_IMPORTED_MODULE_4__["VeureActivitatsComponent"], _inscriure_activitats_inscriure_activitats_component__WEBPACK_IMPORTED_MODULE_5__["InscriureActivitatsComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _suscriptor_routing_module__WEBPACK_IMPORTED_MODULE_2__["SuscriptorRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](SuscriptorModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          declarations: [_suscriptor_component__WEBPACK_IMPORTED_MODULE_3__["SuscriptorComponent"], _veure_activitats_veure_activitats_component__WEBPACK_IMPORTED_MODULE_4__["VeureActivitatsComponent"], _inscriure_activitats_inscriure_activitats_component__WEBPACK_IMPORTED_MODULE_5__["InscriureActivitatsComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _suscriptor_routing_module__WEBPACK_IMPORTED_MODULE_2__["SuscriptorRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/suscriptor/veure-activitats/veure-activitats.component.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/suscriptor/veure-activitats/veure-activitats.component.ts ***!
    \***************************************************************************/

  /*! exports provided: VeureActivitatsComponent */

  /***/
  function srcAppSuscriptorVeureActivitatsVeureActivitatsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "VeureActivitatsComponent", function () {
      return VeureActivitatsComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _app_auth_services_inscripcions_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @app/auth/services/inscripcions.service */
    "./src/app/auth/services/inscripcions.service.ts");
    /* harmony import */


    var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/fire/firestore */
    "./node_modules/@angular/fire/__ivy_ngcc__/fesm2015/angular-fire-firestore.js");
    /* harmony import */


    var _app_auth_services_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @app/auth/services/auth.service */
    "./src/app/auth/services/auth.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    function VeureActivitatsComponent_div_1_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, " Encara no has fet cap inscripci\xF3. Les teves inscripcions es mostraran aqu\xED. ");

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
    }

    function VeureActivitatsComponent_div_2_Template(rf, ctx) {
      if (rf & 1) {
        var _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "a", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function VeureActivitatsComponent_div_2_Template_a_click_3_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r5);

          var inscripcioInfo_r3 = ctx.$implicit;

          var ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r4.onEdit(inscripcioInfo_r3);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](4, "i", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "a", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function VeureActivitatsComponent_div_2_Template_a_click_5_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r5);

          var inscripcioInfo_r3 = ctx.$implicit;

          var ctx_r6 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();

          return ctx_r6.onDelete(inscripcioInfo_r3);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "i", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var inscripcioInfo_r3 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](inscripcioInfo_r3.nomActivitat);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](inscripcioInfo_r3.nomNen);
      }
    }

    function VeureActivitatsComponent_div_3_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "div", 14);
      }
    }

    var VeureActivitatsComponent = /*#__PURE__*/function () {
      function VeureActivitatsComponent(service, fireStore, authSvc) {
        _classCallCheck(this, VeureActivitatsComponent);

        this.service = service;
        this.fireStore = fireStore;
        this.authSvc = authSvc;
      }

      _createClass(VeureActivitatsComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
            var _this2 = this;

            var user;
            return regeneratorRuntime.wrap(function _callee3$(_context3) {
              while (1) {
                switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.next = 2;
                    return this.authSvc.getCurrentUser();

                  case 2:
                    user = _context3.sent;
                    this.userNOW = user.email; //Recullo les dades de les inscripcions filtrades per la variable userNOW (usuari amb la sessió iniciada)

                    this.fireStore.collection('inscripcions', function (ref) {
                      return ref.where("email", "==", _this2.userNOW);
                    }).snapshotChanges().subscribe(function (response) {
                      _this2.inscripcionsLlista = response.map(function (document) {
                        return Object.assign({
                          id: document.payload.doc.id
                        }, document.payload.doc.data());
                      }); // Ordeno els resultats en ordre ascendent

                      _this2.inscripcionsLlista = _this2.inscripcionsLlista.sort(function (obj1, obj2) {
                        return obj1.rollNo - obj2.rollNo;
                      });
                    });

                  case 5:
                  case "end":
                    return _context3.stop();
                }
              }
            }, _callee3, this);
          }));
        }
      }, {
        key: "onEdit",
        value: function onEdit(activitat) {
          this.service.formDataInscripcions = Object.assign({}, activitat);
        }
      }, {
        key: "onDelete",
        value: function onDelete(student) {
          this.fireStore.doc('inscripcions/' + student.id)["delete"]();
          this.deleteMessage = 'La informació de ' + student.nomActivitat + ' ha sigut eliminada correctament';
        }
      }]);

      return VeureActivitatsComponent;
    }();

    VeureActivitatsComponent.ɵfac = function VeureActivitatsComponent_Factory(t) {
      return new (t || VeureActivitatsComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_app_auth_services_inscripcions_service__WEBPACK_IMPORTED_MODULE_2__["InscripcionsService"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_app_auth_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]));
    };

    VeureActivitatsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
      type: VeureActivitatsComponent,
      selectors: [["app-veure-activitats"]],
      decls: 4,
      vars: 3,
      consts: [[1, "caixaActivitats"], ["class", "avisNoInscripcions", 4, "ngIf"], ["class", "caixaOpcio", 4, "ngFor", "ngForOf"], ["class", "caixaOpcioFinal", 4, "ngIf"], [1, "avisNoInscripcions"], [1, "caixaOpcio"], [1, "caixaActivitatNom"], [1, "caixaBotons"], [1, "botoEditar", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-pencil"], [1, "botoEliminar", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-trash"], [1, "caixaActivitatTitol"], [1, "caixaActivitatNen"], [1, "caixaOpcioFinal"]],
      template: function VeureActivitatsComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, VeureActivitatsComponent_div_1_Template, 2, 0, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, VeureActivitatsComponent_div_2_Template, 11, 2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](3, VeureActivitatsComponent_div_3_Template, 1, 0, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx.inscripcionsLlista || ctx.inscripcionsLlista.length === 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.inscripcionsLlista);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", !ctx.inscripcionsLlista || ctx.inscripcionsLlista.length > 0);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_5__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_5__["NgForOf"]],
      styles: [".caixaActivitats[_ngcontent-%COMP%] {\n    display: flex;\n    width: 100%;\n    height: auto;\n    overflow-x: scroll;\n    \n}\n\ni[_ngcontent-%COMP%] {\n    font-size: 20px;\n}\n\n.avisNoInscripcions[_ngcontent-%COMP%] {\n    display: flex;\n    height: 50px;\n    justify-content: center;\n    align-items: center;\n    font-size: 15px;\n    text-align: center;\n    border-radius: 5px;\n    height: 100px;\n    width: 100%;\n    margin: 10px;\n    background-color: grey;\n    color: white;\n    padding: 10px;\n    \n}\n\n.caixaOpcio[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    margin: 10px;\n    min-width: 300px;\n    max-width: 300px;\n    box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.05);\n    border-radius: 5px;\n    background-color: white;\n    justify-content: space-between;\n    overflow: hidden;\n}\n\n.caixaOpcioFinal[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    border: 1px solid white;\n    margin: 10px;\n    width: 20px;    \n    color: white\n}\n\n.caixaActivitatTitol[_ngcontent-%COMP%] {\n    display: flex;\n    width: 100%;\n    height: 100px;\n    font-size: 30px;\n    justify-content: center;\n    align-items: center;\n    font-weight: bold;\n    padding: 10px;\n    margin-bottom: 20px;\n    color: black;\n}\n\n.caixaActivitatNom[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    width: 100%;\n    height: 70%;\n}\n\n.caixaActivitatNen[_ngcontent-%COMP%] {\n    display: flex;\n    height: 30%;\n    width: 100%;\n    font-size: 20px;\n    justify-content: center;\n    align-items: center;\n    font-weight: bold;\n    padding: 10px;\n    background-color: blueviolet;\n    color: white;\n    overflow: hidden;\n\n}\n\n.caixaBotons[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: row;\n    justify-content: flex-end;\n    margin: 15px 15px 0px 15px;\n\n}\n\n.botoEditar[_ngcontent-%COMP%] {\n    display: flex;\n    background-color: white;\n    width: 35px;\n    justify-content: center;\n    align-items: center;\n    height: 35px;\n    \n    box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.05);\n    border-radius: 5px;\n    color: grey;\n\n}\n\n.botoEliminar[_ngcontent-%COMP%] {\n    display: flex;\n    background-color: white;\n    width: 35px;\n    justify-content: center;\n    align-items: center;\n    height: 35px;\n    \n    box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.05);\n    border-radius: 5px;\n    color: grey;\n    margin-left: 15px;\n}\n\n.botoEditar[_ngcontent-%COMP%]:hover {\n    display: flex;\n    background-color: white;\n    width: 35px;\n    justify-content: center;\n    align-items: center;\n    height: 35px;\n    \n    box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.05);\n    border-radius: 5px;\n    color:  rgb(189, 189, 189);\n    cursor: pointer;\n}\n\n.botoEliminar[_ngcontent-%COMP%]:hover {\n    display: flex;\n    background-color: white;\n    width: 35px;\n    justify-content: center;\n    align-items: center;\n    height: 35px;\n    \n    box-shadow: 0px 0px 10px 5px rgba(0,0,0,0.05);\n    border-radius: 5px;\n    color: lightcoral;\n    margin-left: 15px;\n    cursor: pointer;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc3VzY3JpcHRvci92ZXVyZS1hY3Rpdml0YXRzL3ZldXJlLWFjdGl2aXRhdHMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGFBQWE7SUFDYixXQUFXO0lBQ1gsWUFBWTtJQUNaLGtCQUFrQjtJQUNsQiw2QkFBNkI7QUFDakM7O0FBRUE7SUFDSSxlQUFlO0FBQ25COztBQUVBO0lBQ0ksYUFBYTtJQUNiLFlBQVk7SUFDWix1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLGFBQWE7SUFDYixXQUFXO0lBQ1gsWUFBWTtJQUNaLHNCQUFzQjtJQUN0QixZQUFZO0lBQ1osYUFBYTs7QUFFakI7O0FBRUE7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLDZDQUE2QztJQUM3QyxrQkFBa0I7SUFDbEIsdUJBQXVCO0lBQ3ZCLDhCQUE4QjtJQUM5QixnQkFBZ0I7QUFDcEI7O0FBRUE7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2QixZQUFZO0lBQ1osV0FBVztJQUNYO0FBQ0o7O0FBRUE7SUFDSSxhQUFhO0lBQ2IsV0FBVztJQUNYLGFBQWE7SUFDYixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsYUFBYTtJQUNiLG1CQUFtQjtJQUNuQixZQUFZO0FBQ2hCOztBQUVBO0lBQ0ksYUFBYTtJQUNiLHNCQUFzQjtJQUN0QixXQUFXO0lBQ1gsV0FBVztBQUNmOztBQUdBO0lBQ0ksYUFBYTtJQUNiLFdBQVc7SUFDWCxXQUFXO0lBQ1gsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsaUJBQWlCO0lBQ2pCLGFBQWE7SUFDYiw0QkFBNEI7SUFDNUIsWUFBWTtJQUNaLGdCQUFnQjs7QUFFcEI7O0FBRUE7SUFDSSxhQUFhO0lBQ2IsbUJBQW1CO0lBQ25CLHlCQUF5QjtJQUN6QiwwQkFBMEI7O0FBRTlCOztBQUVBO0lBQ0ksYUFBYTtJQUNiLHVCQUF1QjtJQUN2QixXQUFXO0lBQ1gsdUJBQXVCO0lBQ3ZCLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osOEJBQThCO0lBQzlCLDZDQUE2QztJQUM3QyxrQkFBa0I7SUFDbEIsV0FBVzs7QUFFZjs7QUFFQTtJQUNJLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsV0FBVztJQUNYLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLDhCQUE4QjtJQUM5Qiw2Q0FBNkM7SUFDN0Msa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxpQkFBaUI7QUFDckI7O0FBRUE7SUFDSSxhQUFhO0lBQ2IsdUJBQXVCO0lBQ3ZCLFdBQVc7SUFDWCx1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLFlBQVk7SUFDWiw4QkFBOEI7SUFDOUIsNkNBQTZDO0lBQzdDLGtCQUFrQjtJQUNsQiwwQkFBMEI7SUFDMUIsZUFBZTtBQUNuQjs7QUFFQTtJQUNJLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsV0FBVztJQUNYLHVCQUF1QjtJQUN2QixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLDhCQUE4QjtJQUM5Qiw2Q0FBNkM7SUFDN0Msa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixpQkFBaUI7SUFDakIsZUFBZTtBQUNuQiIsImZpbGUiOiJzcmMvYXBwL3N1c2NyaXB0b3IvdmV1cmUtYWN0aXZpdGF0cy92ZXVyZS1hY3Rpdml0YXRzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY2FpeGFBY3Rpdml0YXRzIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogYXV0bztcbiAgICBvdmVyZmxvdy14OiBzY3JvbGw7XG4gICAgLyogYm9yZGVyOiAxcHggc29saWQgZ3JlZW47ICovXG59XG5cbmkge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuLmF2aXNOb0luc2NyaXBjaW9ucyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBmb250LXNpemU6IDE1cHg7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbjogMTBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBncmV5O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBwYWRkaW5nOiAxMHB4O1xuICAgIFxufVxuXG4uY2FpeGFPcGNpbyB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIG1hcmdpbjogMTBweDtcbiAgICBtaW4td2lkdGg6IDMwMHB4O1xuICAgIG1heC13aWR0aDogMzAwcHg7XG4gICAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4IDVweCByZ2JhKDAsMCwwLDAuMDUpO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuLmNhaXhhT3BjaW9GaW5hbCB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIHdoaXRlO1xuICAgIG1hcmdpbjogMTBweDtcbiAgICB3aWR0aDogMjBweDsgICAgXG4gICAgY29sb3I6IHdoaXRlXG59XG5cbi5jYWl4YUFjdGl2aXRhdFRpdG9sIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwcHg7XG4gICAgZm9udC1zaXplOiAzMHB4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICAgIGNvbG9yOiBibGFjaztcbn1cblxuLmNhaXhhQWN0aXZpdGF0Tm9tIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiA3MCU7XG59XG5cblxuLmNhaXhhQWN0aXZpdGF0TmVuIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGhlaWdodDogMzAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHBhZGRpbmc6IDEwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogYmx1ZXZpb2xldDtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcblxufVxuXG4uY2FpeGFCb3RvbnMge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICAgIG1hcmdpbjogMTVweCAxNXB4IDBweCAxNXB4O1xuXG59XG5cbi5ib3RvRWRpdGFyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIHdpZHRoOiAzNXB4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgaGVpZ2h0OiAzNXB4O1xuICAgIC8qIGJvcmRlcjogMS41cHggc29saWQgZ3JleTsgKi9cbiAgICBib3gtc2hhZG93OiAwcHggMHB4IDEwcHggNXB4IHJnYmEoMCwwLDAsMC4wNSk7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGNvbG9yOiBncmV5O1xuXG59XG5cbi5ib3RvRWxpbWluYXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgd2lkdGg6IDM1cHg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBoZWlnaHQ6IDM1cHg7XG4gICAgLyogYm9yZGVyOiAxLjVweCBzb2xpZCBncmV5OyAqL1xuICAgIGJveC1zaGFkb3c6IDBweCAwcHggMTBweCA1cHggcmdiYSgwLDAsMCwwLjA1KTtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgY29sb3I6IGdyZXk7XG4gICAgbWFyZ2luLWxlZnQ6IDE1cHg7XG59XG5cbi5ib3RvRWRpdGFyOmhvdmVyIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xuICAgIHdpZHRoOiAzNXB4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgaGVpZ2h0OiAzNXB4O1xuICAgIC8qIGJvcmRlcjogMS41cHggc29saWQgZ3JleTsgKi9cbiAgICBib3gtc2hhZG93OiAwcHggMHB4IDEwcHggNXB4IHJnYmEoMCwwLDAsMC4wNSk7XG4gICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIGNvbG9yOiAgcmdiKDE4OSwgMTg5LCAxODkpO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuLmJvdG9FbGltaW5hcjpob3ZlciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgICB3aWR0aDogMzVweDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGhlaWdodDogMzVweDtcbiAgICAvKiBib3JkZXI6IDEuNXB4IHNvbGlkIGdyZXk7ICovXG4gICAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4IDVweCByZ2JhKDAsMCwwLDAuMDUpO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBjb2xvcjogbGlnaHRjb3JhbDtcbiAgICBtYXJnaW4tbGVmdDogMTVweDtcbiAgICBjdXJzb3I6IHBvaW50ZXI7XG59XG5cblxuXG4iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵsetClassMetadata"](VeureActivitatsComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"],
        args: [{
          selector: 'app-veure-activitats',
          templateUrl: './veure-activitats.component.html',
          styleUrls: ['./veure-activitats.component.css']
        }]
      }], function () {
        return [{
          type: _app_auth_services_inscripcions_service__WEBPACK_IMPORTED_MODULE_2__["InscripcionsService"]
        }, {
          type: _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_3__["AngularFirestore"]
        }, {
          type: _app_auth_services_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"]
        }];
      }, null);
    })();
    /***/

  }
}]);
//# sourceMappingURL=suscriptor-suscriptor-module-es5.js.map