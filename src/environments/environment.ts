// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDIOCRVKqAOehWFFsVM4PC1xYP0dT8oFj0",
    authDomain: "youractivity-d4ae8.firebaseapp.com",
    databaseURL: "https://youractivity-d4ae8.firebaseio.com",
    projectId: "youractivity-d4ae8",
    storageBucket: "youractivity-d4ae8.appspot.com",
    messagingSenderId: "99065403903",
    appId: "1:99065403903:web:1504e0b8e204e5cc356fca",
    measurementId: "G-XX4QVJG2B7"
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
