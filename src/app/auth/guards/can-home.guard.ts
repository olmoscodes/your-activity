import { AuthService } from '@auth/services/auth.service';
import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { tap, map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class CanHomeGuard implements CanActivate {
  constructor(private authSvc: AuthService, private router: Router) {}

//   canActivate(): Observable<boolean> | Promise<boolean> | boolean {
//     return this.authSvc.UserIsLogged;
//   }
// }

canActivate(): Observable<boolean> | Promise<boolean> | boolean {
  return this.authSvc.user$.pipe(
    take(1),
    map((user) => user && this.authSvc.isSuscriptor(user)),
    tap((canView) => {
      if (!canView) {
        this.router.navigate(['/login']);
      }
      else {
        this.router.navigate(['/suscriptor']);
      }
    })
  );
}



// canActivate(): Observable<boolean> | Promise<boolean> | boolean {
//   return this.authSvc.user$.pipe(
//     take(1),
//     map((user) => user && this.authSvc.getCurrentUser()),
//     tap((canView) => {
//       if (!canView) {
//         this.router.navigate(['/login']);
//       }
//       else {
//         this.router.navigate(['/suscriptor']);
//       }
//     })
//   );
// }
}