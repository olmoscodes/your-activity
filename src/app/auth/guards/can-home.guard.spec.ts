import { TestBed } from '@angular/core/testing';

import { CanHomeGuard } from './can-home.guard';

describe('CanHomeGuard', () => {
  let guard: CanHomeGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CanHomeGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
