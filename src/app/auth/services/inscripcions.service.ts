import { Injectable } from '@angular/core';

import { Inscripcions } from './inscripcions.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})


export class InscripcionsService {

  constructor(private fireStore: AngularFirestore, public authSvc: AuthService) { }

  // Student form data.
  formDataInscripcions: Inscripcions;

  // Fetch all students information.
  getAllInscripcions() {
    return this.fireStore.collection('inscripcions', ref => ref.orderBy("nomActivitat")).snapshotChanges();

  }


}
