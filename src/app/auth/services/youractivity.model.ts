export class Activitats {
    id: string;
    nomActivitat: string;
    nomMonitor: string;
    descripcioActivitat: string;
    horariActivitatInici: string;
    horariActivitatFinal: string;
    preuActivitat: string;
}
