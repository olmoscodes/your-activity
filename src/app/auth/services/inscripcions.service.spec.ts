import { TestBed } from '@angular/core/testing';

import { InscripcionsService } from './inscripcions.service';

describe('InscripcionsService', () => {
  let service: InscripcionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InscripcionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
