export class Inscripcions {
    id: string;
    email: string;
    nomActivitat: string;
    idActivitat: string;
    nomNen: string;
    cognomsNen: string;
    curs: string;
    telf: string;
    nomTutor: string;
}
