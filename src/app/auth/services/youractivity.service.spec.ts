import { TestBed } from '@angular/core/testing';

import { ActivitatsService } from './youractivity.service';

describe('StudentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActivitatsService = TestBed.get(ActivitatsService);
    expect(service).toBeTruthy();
  });
});
