import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import { InscripcionsService } from '@app/auth/services/inscripcions.service';
import { Inscripcions } from 'src/app/auth/services/inscripcions.model';


@Component({
  selector: 'app-inscripcions',
  templateUrl: './inscripcions.component.html',
  styleUrls: ['./inscripcions.component.css']
})
export class InscripcionsComponent implements OnInit {

  inscripcionsList: Inscripcions [];

  constructor(private service: InscripcionsService, private fireStore: AngularFirestore) { }

  ngOnInit() {
    this.service.getAllInscripcions().subscribe(response => {
      this.inscripcionsList = response.map(document => {
        return {
          id: document.payload.doc.id,
          ...document.payload.doc.data() as {}    
        } as Inscripcions;
      })

    });
  }


}
