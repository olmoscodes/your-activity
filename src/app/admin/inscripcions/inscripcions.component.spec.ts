import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InscripcionsComponent } from './inscripcions.component';

describe('InscripcionsComponent', () => {
  let component: InscripcionsComponent;
  let fixture: ComponentFixture<InscripcionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscripcionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InscripcionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
