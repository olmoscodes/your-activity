import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestioActComponent } from './gestio-act.component';

describe('GestioActComponent', () => {
  let component: GestioActComponent;
  let fixture: ComponentFixture<GestioActComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestioActComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestioActComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
