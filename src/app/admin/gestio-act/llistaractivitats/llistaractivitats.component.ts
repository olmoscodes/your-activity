import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import { ActivitatsService } from '@app/auth/services/youractivity.service';
import { Activitats } from 'src/app/auth/services/youractivity.model';

@Component({
  selector: 'app-llistaractivitats',
  templateUrl: './llistaractivitats.component.html',
  styleUrls: ['./llistaractivitats.component.css']
})
export class StudentListComponent implements OnInit {

  deleteMessage: string;
  activitatsLlista: Activitats[];
  constructor(private service: ActivitatsService, private fireStore: AngularFirestore) { }

  ngOnInit() {
    this.service.getAllStudents().subscribe(response => {
      this.activitatsLlista = response.map(document => {
        return {
          id: document.payload.doc.id,
          ...document.payload.doc.data() as {}    
        } as Activitats;
      })

      this.activitatsLlista = this.activitatsLlista.sort((obj1, obj2) => (obj1 as any).rollNo - (obj2 as any).rollNo);
    });
  }

  onEdit(activitat: Activitats) {
    this.service.formData = Object.assign({}, activitat);
  }

  onDelete(student: Activitats) {
    this.fireStore.doc('activitats/' + student.id).delete();
    this.deleteMessage = 'La informació de ' + student.nomActivitat + ' ha sigut eliminada correctament';
  }
}


