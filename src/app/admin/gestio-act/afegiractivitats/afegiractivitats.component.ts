import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import { ActivitatsService } from '@app/auth/services/youractivity.service';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-afegiractivitats',
  templateUrl: './afegiractivitats.component.html',
  styleUrls: ['./afegiractivitats.component.css']
})
export class StudentComponent implements OnInit {

  message: string;
  constructor(public service: ActivitatsService, private fireStore: AngularFirestore) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null) {
      form.resetForm();
    }
    this.service.formData = {
      id: null,
      nomActivitat: '',
      nomMonitor: '',
      descripcioActivitat: '',
      horariActivitatInici: '',
      horariActivitatFinal: '',
      preuActivitat: ''
    }
  }

  onSubmit(form: NgForm) {
    this.message = '';

    let ActivitatDades = Object.assign({}, form.value);

    delete ActivitatDades.id;

    if (form.value.id == null) {
      this.fireStore.collection('activitats').add(ActivitatDades);
      this.message = ActivitatDades.nomActivitat + ' ha sigut guardat/a correctament ';
    } else {
      this.fireStore.doc('activitats/' + form.value.id).update(ActivitatDades);
      this.message = 'Activitat editada correctament';
    }

    this.resetForm(form);
  }
}
