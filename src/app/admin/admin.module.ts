import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { GestioActComponent } from './gestio-act/gestio-act.component';
import { StudentComponent } from './gestio-act/afegiractivitats/afegiractivitats.component';
import { StudentListComponent } from './gestio-act/llistaractivitats/llistaractivitats.component';
import { FormsModule } from '@angular/forms';
import { ComptabilitatComponent } from './comptabilitat/comptabilitat.component';
import { InscripcionsComponent } from './inscripcions/inscripcions.component';


@NgModule({
  declarations: [AdminComponent, GestioActComponent, StudentComponent, StudentListComponent, ComptabilitatComponent, InscripcionsComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule
  ]
})
export class AdminModule { }
