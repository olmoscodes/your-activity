import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComptabilitatComponent } from './comptabilitat.component';

describe('ComptabilitatComponent', () => {
  let component: ComptabilitatComponent;
  let fixture: ComponentFixture<ComptabilitatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComptabilitatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComptabilitatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
