import { Component, OnInit } from '@angular/core';
import { InscripcionsService } from '@app/auth/services/inscripcions.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Inscripcions } from 'src/app/auth/services/inscripcions.model';
import { AuthService } from '@app/auth/services/auth.service';


@Component({
  selector: 'app-veure-activitats',
  templateUrl: './veure-activitats.component.html',
  styleUrls: ['./veure-activitats.component.css']
})
export class VeureActivitatsComponent implements OnInit {

  inscripcionsLlista: Inscripcions [];
  userNOW: string;
  deleteMessage: string;


  constructor(private service: InscripcionsService, private fireStore: AngularFirestore, public authSvc: AuthService) { }

  async ngOnInit() {

    // Torno a extreure quin usuari está amb la sessió iniciada i el fico dintre de la variable userNOW
    
    const user = await this.authSvc.getCurrentUser();
    this.userNOW = user.email;
    //Recullo les dades de les inscripcions filtrades per la variable userNOW (usuari amb la sessió iniciada)

    
    this.fireStore.collection('inscripcions', ref => ref.where("email", "==", this.userNOW)).snapshotChanges().subscribe(response => {
      this.inscripcionsLlista = response.map(document => {
        return {
          id: document.payload.doc.id,
          ...document.payload.doc.data() as {}    
        } as Inscripcions;
      })

      // Ordeno els resultats en ordre ascendent
      this.inscripcionsLlista = this.inscripcionsLlista.sort((obj1, obj2) => (obj1 as any).rollNo - (obj2 as any).rollNo);
    });

    



  }

  onEdit(activitat: Inscripcions) {
    this.service.formDataInscripcions = Object.assign({}, activitat);
  }

  onDelete(student: Inscripcions) {
    this.fireStore.doc('inscripcions/' + student.id).delete();
    this.deleteMessage = 'La informació de ' + student.nomActivitat + ' ha sigut eliminada correctament';
  }


}
