import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VeureActivitatsComponent } from './veure-activitats.component';

describe('VeureActivitatsComponent', () => {
  let component: VeureActivitatsComponent;
  let fixture: ComponentFixture<VeureActivitatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VeureActivitatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VeureActivitatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
