import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InscriureActivitatsComponent } from './inscriure-activitats.component';

describe('InscriureActivitatsComponent', () => {
  let component: InscriureActivitatsComponent;
  let fixture: ComponentFixture<InscriureActivitatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InscriureActivitatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InscriureActivitatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
