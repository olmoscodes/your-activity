import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';


import { ActivitatsService } from '@app/auth/services/youractivity.service';
import { InscripcionsService } from '@app/auth/services/inscripcions.service';

import { Activitats } from 'src/app/auth/services/youractivity.model';
import { AuthService } from '@app/auth/services/auth.service';
import { NgForm } from '@angular/forms';




@Component({
  selector: 'app-inscriure-activitats',
  templateUrl: './inscriure-activitats.component.html',
  styleUrls: ['./inscriure-activitats.component.css'],
  providers: [AuthService],
})
export class InscriureActivitatsComponent implements OnInit {
  deleteMessage: string;

  ActivitatsLlista: Activitats[];

  emailRegistrat: string;

  message: string;



  constructor ( public service: ActivitatsService, private fireStore: AngularFirestore, public serviceInscripcions: InscripcionsService,
                public authSvc: AuthService) { }

  async ngOnInit() {

    // Cada vegada que carrego la pàgina actualitzo el formulari
    this.resetForm();
    
    // Agafo l'usuari que està actualment registrat
    const user = await this.authSvc.getCurrentUser();
    // El fico en una variable per que quedi registrat qui registra als nens
    this.emailRegistrat = user.email;

    
    //Afago totes les activitats per ficar-les com a input en el formulari
    this.service.getAllStudents().subscribe(response => {
      this.ActivitatsLlista = response.map(document => {
        return {
          id: document.payload.doc.id,
          ...document.payload.doc.data() as {}    // Attention - Require typescript version >3 to work!! AHORA ENTIENDO POR QUE NO IBA
        } as Activitats;
      })

      // Sorting the student-list in ascending order
      this.ActivitatsLlista = this.ActivitatsLlista.sort((obj1, obj2) => (obj1 as any).rollNo - (obj2 as any).rollNo);
    });
  }
  
  // Funció de resetejar el formulari
  async resetForm(form?: NgForm) {
    if (form != null) {
      form.resetForm();
    }
    this.serviceInscripcions.formDataInscripcions = {
      id: null,
      idActivitat: '',
      nomActivitat: '',
      nomNen: '',
      cognomsNen: '',
      curs: '',
      nomTutor: '',
      telf: '',
      email: ''    
    }
  
    // Torno a ficar el id user dintre de la variable, ja que quan formatejava el formulari sense tornar a carregar
    // la pàgina la següent inserció a la taula de inscripción em sortia email: null. Això ho arregla.
    const user = await this.authSvc.getCurrentUser();
    this.emailRegistrat = user.email;
  }

  // Funció per incloure informació a al bbdd a partir del formulari
  onSubmit(form: NgForm) {
    // Reset the message value.
    this.message = '';

    // Making the copy of the form and assigning it to the studentData.
    let InscripcioData = Object.assign({}, form.value);

    // To avoid messing up the document id and just update the other details of the student. We will remove the 'property' from the student data.
    delete InscripcioData.id;

    // Does the insert operation.
    if (form.value.id == null) {
      this.fireStore.collection('inscripcions').add(InscripcioData);
      this.message = InscripcioData.nomActivitat + ' ha sigut guardat/a correctament ';
    } else {
      // Does the update operation for the selected student.
      // The 'studentData' object has the updated details of the student.
      this.fireStore.doc('inscripcions/' + form.value.id).update(InscripcioData);
      this.message = 'Inscripcions editada correctament';
    }

    // Reset the form if the operation is successful.
    this.resetForm(form);

  }


}
