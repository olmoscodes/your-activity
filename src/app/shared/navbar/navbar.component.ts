import { User } from '@shared/models/user.interface';
import { Observable } from 'rxjs';
import { AuthService } from '@auth/services/auth.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent {
  public user$: Observable<User> = this.authSvc.afAuth.user;
  public adminOrNot: any;

  public navbarvisible = false;
  public botoamaga = false;
  public botomostra = true;

  constructor(public authSvc: AuthService, private router: Router) {}

  // Amb això evitem l'entrada a diferents 
  async ngOnInit() {
    this.adminOrNot = await this.authSvc.getCurrentUser();
    if (this.adminOrNot) {
      console.log('hi ha usuari amb sessió iniciada');
    }
    else {
      this.router.navigate(['/login']);
    }
  }

  async onLogout() {
    try {
      await this.authSvc.logout();
      this.router.navigate(['/login']);
    } catch (error) {
      console.log(error);
    }
  }


}
